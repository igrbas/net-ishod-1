#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Diary.Models;

namespace Diary.Pages_Articles {
    public class FavouritesModel : PageModel {
        private readonly DiaryDbContext _context;

        public FavouritesModel(DiaryDbContext context) {
            _context = context;
        }

        public IList<Article> Article { get; set; }

        public async Task OnGetAsync() {
            Article = await _context.Article.Where(a => a.Favourite == true).ToListAsync();
        }

        public async Task<IActionResult> OnDeleteAsync(int? id) {
            if (id == null) {
                return NotFound();
            }

            Article article = await _context.Article.FindAsync(id);
            if (article != null) {
                _context.Article.Remove(article);
                await _context.SaveChangesAsync();
            }

            return new OkResult();
        }
    }
}
