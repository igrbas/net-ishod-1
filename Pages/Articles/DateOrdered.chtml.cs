#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Diary.Models;

namespace Diary.Pages_Articles {
    public class DateOrderedModel : PageModel {
        private readonly DiaryDbContext _context;

        public DateOrderedModel(DiaryDbContext context) {
            _context = context;
        }

        public IList<Article> Article { get; set; }

        public async Task OnGetAsync() {
            Article = await _context.Article.OrderBy(a => a.Date).ToListAsync();
        }
    }
}
