#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Diary.Models;

    public class DiaryDbContext : DbContext
    {
        public DiaryDbContext (DbContextOptions<DiaryDbContext> options)
            : base(options)
        {
        }

        public DbSet<Diary.Models.Article> Article { get; set; }
    }
