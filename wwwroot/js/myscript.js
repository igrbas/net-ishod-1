function deleteArticle(id, name) {
    Swal.fire({
        title: `Jeste li sigurni da želite obrisati "${name}" ?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Obriši',
        cancelButtonText: 'Odustani'
    }).then((result) => {
        if(result.isConfirmed){
            $.ajax({
                url: '/articles/index',
                type: 'DELETE',
                headers: { RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
                data: { 'id': id }
            }).done((result) => {
                window.location.reload();
            });
        }
    });
}

function changeFavourite(id) {
    $.ajax({
        url: '/articles/index',
        type: 'PUT',
        headers: { RequestVerificationToken: $('input:hidden[name="__RequestVerificationToken"]').val() },
        data: { 'id': id }
    }).done((result) => {
        $(`#${id}-star`).toggleClass("bi-star");
        $(`#${id}-star`).toggleClass("bi-star-fill");
    });
}
